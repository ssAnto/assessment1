package com.ssanto.news

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ssanto.news.adapter.ArticleAdapter
import com.ssanto.news.model.ArticleModel
import com.ssanto.news.model.ResponseBodyArticle
import com.ssanto.news.service.APIUtilities
import com.ssanto.news.service.RetrofitAPIService
import com.ssanto.news.utils.Constants
import com.ssanto.news.utils.Loading
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ArticleActivity : AppCompatActivity() {

    private var source:String = ""
    private var pagenow = 1
    lateinit var recyclerView: RecyclerView
    lateinit var loading:ProgressDialog
    private var adapter :ArticleAdapter?=null
    private var endofpage:Boolean = false
    private var context:Context = this

    lateinit var search:ImageButton
    lateinit var keyword:EditText

    private var keyword_tmp=""

    private var data:ArrayList<ArticleModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

        source = intent.extras!!.getString(Constants.SOURCE).toString()
        loading = Loading.LoadingStandar(context = this)!!

        recyclerView = findViewById(R.id.articleRecycler)

        search = findViewById(R.id.search_news)
        keyword=findViewById(R.id.search_news_keyword)

        search.setOnClickListener(View.OnClickListener {
            data.clear()
            if(keyword.text.toString().length > 0){
                keyword_tmp = keyword.text.toString()
                pagenow=1
                endofpage=false
                getDataArticle()
            }else{
                Toast.makeText(context,"What kind of news do you want ? ", Toast.LENGTH_SHORT).show()
            }
        })

        recyclerView.setOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (!endofpage) {
                        getDataArticle()
                    }
                }
            }
        })

        getDataArticle()

    }

    private fun getDataArticle() {
        var service : RetrofitAPIService = APIUtilities().getAPIServices()!!
        loading!!.show()
        if(keyword_tmp.length>0){
            service.getArticles(
                Constants.API_KEY,
                source,
                25,
                pagenow,
                keyword_tmp
            ).enqueue(object : Callback<ResponseBodyArticle?> {
                override fun onResponse(
                    call: Call<ResponseBodyArticle?>?,
                    response: Response<ResponseBodyArticle?>
                ) {
                    loading!!.dismiss()
                    if(response.code()==200){
                        if(response.body()!!.status.equals("ok", ignoreCase = true)){
                            if(response.body()!!.articles?.size!! >0) {
                                for (i in 0..(response.body()!!.articles?.size!!-1)) {
                                    data.add(response.body()!!.articles!![i])
                                }
                                pagenow++
                                initList()
                            }else{
                                endofpage = true
                                Toast.makeText(context,"Article is Not Available for source ${source}", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }else{
                        Toast.makeText(context,"Error : ${response.code()}", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseBodyArticle?>?, t: Throwable) {
                    loading!!.dismiss()
                    Toast.makeText(context,"Error : ${t.message}", Toast.LENGTH_SHORT).show()
                }
            } )
        }else{
            service.getArticles(
                Constants.API_KEY,
                source,
                25,
                pagenow,
            ).enqueue(object : Callback<ResponseBodyArticle?> {
                override fun onResponse(
                    call: Call<ResponseBodyArticle?>?,
                    response: Response<ResponseBodyArticle?>
                ) {
                    loading!!.dismiss()
                    if(response.code()==200){
                        if(response.body()!!.status.equals("ok", ignoreCase = true)){
                            if(response.body()!!.articles?.size!! >0) {
                                for (i in 0..(response.body()!!.articles?.size!!-1)) {
                                    data.add(response.body()!!.articles!![i])
                                }
                                pagenow++
                                initList()
                            }else{
                                endofpage = true
                                Toast.makeText(context,"Article is Not Available for source ${source}", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }else{
                        Toast.makeText(context,"Error : ${response.code()}", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseBodyArticle?>?, t: Throwable) {
                    loading!!.dismiss()
                    Toast.makeText(context,"Error : ${t.message}", Toast.LENGTH_SHORT).show()
                }
            } )
        }

    }


    private fun initList() {
        if (adapter == null) {
            adapter = data?.let { ArticleAdapter( this, it) }

            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = adapter
        }
        adapter!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        finish()
    }
}