package com.ssanto.news.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ssanto.news.R
import com.ssanto.news.viewholder.SourceViewHolder

class SourceAdapter(
    private val context: Context,
    private val data:List<String>
): RecyclerView.Adapter<SourceViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SourceViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.source_item, parent, false)
        return SourceViewHolder(context,view)
    }

    override fun onBindViewHolder(holder: SourceViewHolder, position: Int) {
        holder.setSource(data[position])
    }

    override fun getItemCount(): Int {
       return data.size
    }

}