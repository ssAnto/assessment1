package com.ssanto.news.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ssanto.news.R
import com.ssanto.news.model.ArticleModel
import com.ssanto.news.viewholder.ArticleViewHolder

class ArticleAdapter(
    private val context: Context,
    private val data : List<ArticleModel>
): RecyclerView.Adapter<ArticleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.article_item, parent, false)
        return ArticleViewHolder(context,view)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.setArticle(data[position])
    }

    override fun getItemCount(): Int {
       return data.size
    }
}