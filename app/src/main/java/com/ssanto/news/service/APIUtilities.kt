package com.ssanto.news.service

class APIUtilities {
    fun getAPIServices(): RetrofitAPIService? {
        val tmp = APIUtilities()
        return tmp.getIP()
    }

    private fun getIP(): RetrofitAPIService? {
        val tmp = RetrofitClient()
        return tmp.getClient()!!.create(RetrofitAPIService::class.java)
    }
}