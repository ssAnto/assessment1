package com.ssanto.news.service

import androidx.annotation.Nullable
import com.ssanto.news.model.ResponseBodyArticle
import com.ssanto.news.model.ResponseBodySource
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitAPIService {
    @GET("/v2/top-headlines")
    fun getArticles(
        @Query("apiKey") apiKey:String,
        @Query("sources") source:String,
        @Query("pageSize") pagesize:Int,
        @Query("page") page:Int,
    ): Call<ResponseBodyArticle>

    @GET("/v2/top-headlines/sources")
    fun getSource(
        @Query("apiKey") apiKey:String,
        @Query("category") category:String
    ):Call<ResponseBodySource>

    @GET("/v2/top-headlines")
    fun getArticles(
        @Query("apiKey") apiKey:String,
        @Query("sources") source:String,
        @Query("pageSize") pagesize:Int,
        @Query("page") page:Int,
        @Query("q") keyword:String
    ): Call<ResponseBodyArticle>
}