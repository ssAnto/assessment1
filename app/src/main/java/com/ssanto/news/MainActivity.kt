package com.ssanto.news

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.ssanto.news.utils.Constants

class MainActivity : AppCompatActivity() {


    lateinit var sport:Button
    lateinit var tech:Button
    lateinit var businnes:Button
    lateinit var general:Button
    lateinit var science:Button
    lateinit var entertain:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sport = findViewById(R.id.button)
        tech = findViewById(R.id.button2)
        businnes = findViewById(R.id.button3)

        general = findViewById(R.id.button4)
        science = findViewById(R.id.button5)
        entertain = findViewById(R.id.button7)

        action(sport)
        action(tech)
        action(businnes)
        action(general)
        action(science)
        action(entertain)

    }

    private fun action(button: Button) {
        button.setOnClickListener {
            if(it!=null) {
                var tmp: Button = findViewById(it!!.id)
                var filter = "${tmp.text}"
                var intent = Intent(this, SourceActivity::class.java)
                intent.putExtra(Constants.CATEGORYES, filter)
                startActivity(intent)
            }
        }

    }

}