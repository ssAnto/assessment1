package com.ssanto.news.viewholder

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ssanto.news.R
import com.ssanto.news.WebviewActivity
import com.ssanto.news.model.ArticleModel
import com.ssanto.news.utils.Constants

class ArticleViewHolder(
    private var context: Context,
    itemView : View
) : RecyclerView.ViewHolder(itemView) {

    lateinit var title:TextView
    lateinit var author:TextView
    lateinit var publishad:TextView
    lateinit var desc:TextView
    lateinit var src:TextView
    lateinit var img:ImageView
    lateinit var next:ImageView

    fun setArticle(data:ArticleModel) {
        title = itemView.findViewById(R.id.titlenews)
        author = itemView.findViewById(R.id.author)
        publishad=itemView.findViewById(R.id.publishedAt)
        desc = itemView.findViewById(R.id.description)
        src = itemView.findViewById(R.id.source)
        img = itemView.findViewById(R.id.imagenews)
        next = itemView.findViewById(R.id.imageView2)

        title.text = data.title
        author.text = data.author
        publishad.text = data.publishedAt
        desc.text = data.content
        src.text = data.source!!.name
        Glide.with(context).load(data.urlToImage).into(img)

        next.setOnClickListener {
            var intent = Intent(context,WebviewActivity::class.java)
            intent.putExtra(Constants.URL_DETAIL,data.url)
            context.startActivities(arrayOf(intent))
        }
    }
}