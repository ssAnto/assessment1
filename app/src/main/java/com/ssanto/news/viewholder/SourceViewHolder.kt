package com.ssanto.news.viewholder

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ssanto.news.ArticleActivity
import com.ssanto.news.R
import com.ssanto.news.utils.Constants

class SourceViewHolder(
    private val context: Context,
    itemView: View
):RecyclerView.ViewHolder(itemView) {

    fun setSource(data:String){
        val source:TextView = itemView.findViewById(R.id.source_name)
        source.text = data
        source.setOnClickListener {
            var intent = Intent(context,ArticleActivity::class.java)
            intent.putExtra(Constants.SOURCE,data)
            context.startActivities(arrayOf(intent))
        }
    }
}