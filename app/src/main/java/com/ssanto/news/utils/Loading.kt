package com.ssanto.news.utils

import android.R

import android.app.ProgressDialog
import android.content.Context

object Loading {
    fun LoadingStandar(context: Context): ProgressDialog? {
        val loading = ProgressDialog(context)
        loading.setMessage("Loading ...")
        loading.setCancelable(false)
        return loading
    }
}