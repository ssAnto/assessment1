package com.ssanto.news

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import com.ssanto.news.utils.Constants

class WebviewActivity : AppCompatActivity() {

    lateinit var webview:WebView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)


        var url = intent.extras?.getString(Constants.URL_DETAIL)
        webview = findViewById(R.id.webview1)

        webview.settings.javaScriptEnabled=true
        url?.let { webview.loadUrl(it) }
    }
}