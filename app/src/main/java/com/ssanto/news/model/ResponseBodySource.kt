package com.ssanto.news.model

import com.google.gson.annotations.SerializedName

data class ResponseBodySource(
    @field:SerializedName("status")
    var status:String="",
    @field:SerializedName("sources")
    var sources:ArrayList<DetailSource>
)