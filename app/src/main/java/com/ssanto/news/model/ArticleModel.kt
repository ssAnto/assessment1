package com.ssanto.news.model

import com.google.gson.annotations.SerializedName

data class ArticleModel (
    @field:SerializedName("source")
    var source: SourceModel?=null,
    @field:SerializedName("author")
    var author: String = "",
    @field:SerializedName("title")
    var title : String = "",
    @field:SerializedName("description")
    var description: String = "",
    @field:SerializedName("url")
    var url: String = "",
    @field:SerializedName("urlToImage")
    var urlToImage: String ="",
    @field:SerializedName("publishedAt")
    var publishedAt: String= "",
    @field:SerializedName("content")
    var content:String = ""
)