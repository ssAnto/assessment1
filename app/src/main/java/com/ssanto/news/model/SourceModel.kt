package com.ssanto.news.model

import com.google.gson.annotations.SerializedName

data class SourceModel (
    @field:SerializedName("id")
    var id: String = "",
    @field:SerializedName("name")
    var name:String = ""
)
