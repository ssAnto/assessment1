package com.ssanto.news.model

import com.google.gson.annotations.SerializedName

data class ResponseBodyArticle(
    @field:SerializedName("status")
    var status:String = "",
    @field:SerializedName("totalResults")
    var totalResults:Int = 0,
    @field:SerializedName("articles")
    var articles:ArrayList<ArticleModel>?=null,
    @field:SerializedName("message")
    var message:String="",
    @field:SerializedName("code")
    var code:String=""
)