package com.ssanto.news

import android.app.ProgressDialog
import android.content.Context
import android.media.Image
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ssanto.news.adapter.SourceAdapter
import com.ssanto.news.model.ResponseBodySource
import com.ssanto.news.service.APIUtilities
import com.ssanto.news.service.RetrofitAPIService
import com.ssanto.news.utils.Constants
import com.ssanto.news.utils.Loading
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList


class SourceActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    private var adapter:SourceAdapter?=null

    private var loading:ProgressDialog?=null
    private var category = ""

    private var context: Context = this

    private var data : ArrayList<String> =  ArrayList()

    lateinit var keyword:EditText
    lateinit var search:ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_source)


        category = intent.extras?.getString(Constants.CATEGORYES).toString()
        loading = Loading.LoadingStandar(context = this)

        recyclerView = findViewById(R.id.sourceRecycler)

        keyword=findViewById(R.id.search_source_keyword)
        search=findViewById(R.id.search_source)

        search.setOnClickListener(View.OnClickListener {
            if(keyword.text.toString().length>0){
                Toast.makeText(context,"Sorry, feature search for News Source is Unavailable.",Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(context,"What kind of Source do you need?",Toast.LENGTH_SHORT).show()
            }
        })

        var service:RetrofitAPIService = APIUtilities().getAPIServices()!!
        loading!!.show()
        service.getSource(
           Constants.API_KEY,
            category
        ).enqueue(object : Callback<ResponseBodySource?> {
            override fun onResponse(
                call: Call<ResponseBodySource?>?,
                response: Response<ResponseBodySource?>
            ) {
                loading!!.dismiss()
                if(response.code()==200){
                    if(response.body()!!.status.equals("ok", ignoreCase = true)){
                        if(response.body()!!.sources.size>0) {
                            for (i in 0..(response.body()!!.sources.size-1)) {
                                data.add(response.body()!!.sources[i].name)
                            }
                            setListSource()
                        }else{
                            Toast.makeText(context,"No Source for category ${category}",Toast.LENGTH_SHORT).show()
                        }
                    }
                }else{
                    Toast.makeText(context,"Error : ${response.code()}",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<ResponseBodySource?>?, t: Throwable) {
                loading!!.dismiss()
                Toast.makeText(context,"Error : ${t.message}",Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun setListSource() {
        if (adapter == null) {
            adapter = data?.let { SourceAdapter( this, it) }

            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = adapter
        }
        adapter!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        finish()
    }
}